package com.store.retail.api;

import com.store.retail.models.BillRequestModel;
import com.store.retail.models.BillResponseModel;
import com.store.retail.services.interfaces.BillService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Log4j2
@RequestMapping("/api")
public class BillController {

    @Autowired
    BillService billService;

    @PostMapping(path = "/bill")
    @ApiOperation(value = "Calculate Bill Based in User total bill amount. Calculate payable amount based on user or " +
            "bill amount")
    public ResponseEntity<BillResponseModel> getBillPayableAmount(@Valid @RequestBody BillRequestModel billRequestModel) {
        return new ResponseEntity<BillResponseModel>(billService.getBillPayableAmount(billRequestModel), HttpStatus.OK);
    }
}
