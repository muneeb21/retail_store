package com.store.retail.api;

import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping("/api")
public class HelloController {

    @GetMapping(path = "/")
    @ApiOperation(value = "Just a hello world api")
    public ResponseEntity<String> dumpLogsToEs() {
        return new ResponseEntity<String>("Hello World", HttpStatus.OK);
    }
}
