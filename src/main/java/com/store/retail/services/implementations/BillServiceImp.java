package com.store.retail.services.implementations;

import com.store.retail.constants.Discounts;
import com.store.retail.discount.base.Discount;
import com.store.retail.discount.implementations.AffiliateDiscount;
import com.store.retail.discount.implementations.EmployeeDiscount;
import com.store.retail.discount.implementations.HighBillDiscount;
import com.store.retail.discount.implementations.LoyalCustomerDiscount;
import com.store.retail.models.BillRequestModel;
import com.store.retail.models.BillResponseModel;
import com.store.retail.services.interfaces.BillService;
import org.springframework.stereotype.Service;

@Service
public class BillServiceImp implements BillService {

    public BillResponseModel getBillPayableAmount(BillRequestModel billRequestModel) {
        Discount discount = null;
        if (billRequestModel.getUser().getIsEmployee() && !billRequestModel.getIsGrocery()) {
            discount = new EmployeeDiscount();
        } else if (billRequestModel.getUser().getIsAffiliate() && !billRequestModel.getIsGrocery()) {
            discount = new AffiliateDiscount();
        } else if (billRequestModel.getUser().getCustomerFromYear() > Discounts.LOYAL_CUSTOMER_NUMBER_OF_YEAR
                && !billRequestModel.getIsGrocery()) {
            discount = new LoyalCustomerDiscount();
        } else if (billRequestModel.getBillTotalAmount() > Discounts.HIGH_BILL_DISCOUNT) {
            discount = new HighBillDiscount();
        }
        if (discount != null) {
            Float discountedAmount = discount.calculateDiscount(billRequestModel.getBillTotalAmount());
            Float payableAmount = billRequestModel.getBillTotalAmount() - discountedAmount;
            return new BillResponseModel(payableAmount, billRequestModel.getBillTotalAmount(), discountedAmount);
        }
        return new BillResponseModel(billRequestModel.getBillTotalAmount(), billRequestModel.getBillTotalAmount(), 0f);
    }

}
