package com.store.retail.services.interfaces;

import com.store.retail.models.BillRequestModel;
import com.store.retail.models.BillResponseModel;

public interface BillService {

    BillResponseModel getBillPayableAmount(BillRequestModel billRequestModel);
}
