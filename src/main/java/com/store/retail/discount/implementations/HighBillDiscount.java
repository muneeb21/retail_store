package com.store.retail.discount.implementations;

import com.store.retail.discount.base.Discount;

public class HighBillDiscount extends Discount {

    @Override
    public Float getDiscountPercentage() {
        return 5f;
    }

    public Float calculateDiscount(Float billAmount) {
        double dividedAmount = billAmount / 100;
        double flooredAmount = Math.floor(dividedAmount);
        return 100 * (float) flooredAmount * this.getDiscountPercentage() / 100;
    }

}
