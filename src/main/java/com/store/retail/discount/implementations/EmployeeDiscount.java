package com.store.retail.discount.implementations;

import com.store.retail.discount.base.Discount;

public class EmployeeDiscount extends Discount {

    @Override
    public Float getDiscountPercentage() {
        return 30f;
    }

}
