package com.store.retail.discount.implementations;

import com.store.retail.discount.base.Discount;

public class LoyalCustomerDiscount extends Discount {

    @Override
    public Float getDiscountPercentage() {
        return 5f;
    }
}
