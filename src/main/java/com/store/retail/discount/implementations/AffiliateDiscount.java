package com.store.retail.discount.implementations;

import com.store.retail.discount.base.Discount;

public class AffiliateDiscount extends Discount {

    @Override
    public Float getDiscountPercentage() {
        return 10f;
    }
}
