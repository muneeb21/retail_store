package com.store.retail.discount.base;

public abstract class Discount {

    public abstract Float getDiscountPercentage();

    public Float calculateDiscount(Float billAmount) {
        return billAmount * this.getDiscountPercentage() / 100;
    }
}
