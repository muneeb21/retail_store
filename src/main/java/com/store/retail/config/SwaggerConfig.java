package com.store.retail.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        ArrayList<ApiKey> keys = new ArrayList<>();
        keys.add(new ApiKey("Authorization", "Bearer", "header"));
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.store.retail.api"))
                .paths(PathSelectors.any()).build().apiInfo(apiInfo()).securitySchemes(keys);
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo("Retail Store service REST API", "Retail Store service rest api documentation.", "API 2.0", "Terms of service",
                new Contact("Muneeb", "", "muneeb.ahmed20@gmail.com"), "License of API", "API license URL");
        return apiInfo;
    }
}
