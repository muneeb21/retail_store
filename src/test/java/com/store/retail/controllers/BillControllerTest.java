package com.store.retail.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.store.retail.models.BillRequestModel;
import com.store.retail.models.user.UserModel;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BillControllerTest {

    @Autowired
    private MockMvc mockMvc;


    private BillRequestModel getBillRequestModel(Float amount, Boolean isGrocery, Boolean isEmployee, Boolean isAffiliate,
                                                 Integer customerFromYear, Integer userId) {
        BillRequestModel billRequestModel = new BillRequestModel();
        UserModel userModel = new UserModel();
        userModel.setIsEmployee(isEmployee);
        userModel.setIsAffiliate(isAffiliate);
        userModel.setCustomerFromYear(customerFromYear);
        userModel.setUserId(userId);
        billRequestModel.setBillTotalAmount(amount);
        billRequestModel.setUser(userModel);
        billRequestModel.setIsGrocery(isGrocery);
        return billRequestModel;
    }

    @Test
    public void testBillPayableForEmployee() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(100f, false,
                true, false, 1, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(70.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(100.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(30.0)));
    }

    @Test
    public void testBillPayableForAffiliate() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(100f, false,
                false, true, 1, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(90.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(100.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(10.0)));
    }

    @Test
    public void testBillPayableForLoyalCustomer() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(100f,false,
                false, false, 3, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(95.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(100.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(5.0)));
    }

    @Test
    public void testBillPayableForLargePayment() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(550f,false,
                false, false, 1, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(525.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(550.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(25.0)));
    }

    @Test
    public void testBillPayableForEmployeeWithIsGrocerySmallAmount() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(90f,true,
                true, false, 1, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(90.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(90.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(0.0)));
    }

    @Test
    public void testBillPayableForEmployeeWithIsGroceryLargeAmount() throws Exception {
        BillRequestModel billRequestModel = getBillRequestModel(220f,true,
                true, false, 1, 2);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content(mapper.writeValueAsString(billRequestModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.net_payable_amount", Matchers.is(210.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bill_amount", Matchers.is(220.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.discounted_amount", Matchers.is(10.0)));
    }

    @Test
    public void testBillPayableForBadRequest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/bill").content("")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
